﻿// -----------------------------------------------------------------------
// <copyright file="TransformMoq.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Softserve.Halp.Modules.CalcModule.Abstraction.Proxies
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Moq;
    using Moq.Linq;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public static class Moqs
    {
        static Moqs()
        {
            try
            {
                Initalize();
            }
            catch (Exception)
            {
                /// TODO: Add log audit logic here
            }
        }

        public static ITransformContract TransformProxyMoq { get { return mTransformProxyMoq.Object; } }
        public static IFormatContract FormatProxyMoq { get { return mFormatProxyMoq.Object; } }

        private static void Initalize()
        {
            lock (__mutex)
            {
                initializeFormat();

                initializeTransform();
            }
        }

        private static void initializeTransform()
        {
           
        }

        private static void initializeFormat()
        {

        }

        private static readonly object __mutex = new object();
        private static readonly Mock<IFormatContract> mFormatProxyMoq = new Mock<IFormatContract>(MockBehavior.Loose);
        private static readonly Mock<ITransformContract> mTransformProxyMoq = new Mock<ITransformContract>(MockBehavior.Strict);
    }
}
