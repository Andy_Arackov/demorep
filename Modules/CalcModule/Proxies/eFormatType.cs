﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Softserve.Halp.Modules.CalcModule.Abstraction
{
    public enum eFormatType: byte
    {
        JSON = 0,
        XML = 1,
        Binary = 2,
        CSV = 3
    }
}
